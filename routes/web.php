<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Events\PaymentPayed;

Auth::routes([
    'register' => false,
]);

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('/', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::group(['prefix' => 'leiders'], function () {
        Route::get('/', 'LeaderController@index')->name('leader.index');
        Route::get('create', 'LeaderController@create')->name('leader.create');
        Route::POST('create', 'LeaderController@store')->name('leader.store');
        Route::get('edit/{leader}', 'LeaderController@edit')->name('leader.edit');
        Route::POST('edit/{leader}', 'LeaderController@update')->name('leader.update');
    });

    Route::group(['prefix' => 'kinderen'], function () {
        Route::get('/', 'ChildController@index')->name('child.index');
        Route::get('create', 'ChildController@create')->name('child.create');
        Route::POST('create', 'ChildController@store')->name('child.store');
        Route::get('edit/{child}', 'ChildController@edit')->name('child.edit');
        Route::POST('edit/{child}', 'ChildController@update')->name('child.update');
    });

    Route::group(['prefix' => 'kleuters'], function () {
        Route::get('/', 'PreschoolerController@index')->name('preschooler.index');
        Route::get('create', 'PreschoolerController@create')->name('preschooler.create');
        Route::POST('create', 'PreschoolerController@store')->name('preschooler.store');
        Route::get('edit/{preschooler}', 'PreschoolerController@edit')->name('preschooler.edit');
        Route::POST('edit/{preschooler}', 'PreschoolerController@update')->name('preschooler.update');
    });

    Route::group(['prefix' => 'gebruikers',  'as' => 'user.'], function () {
        Route::get('/', 'UserController@index')->name('overview');
    });

    Route::get('mail', function () {
//        $payment = \App\Models\Payment::find(3);
//        PaymentPayed::dispatch($payment);
//
        $financialActionOrder = \App\Models\FinancialActionOrder::find(8);
        $message = $financialActionOrder->content->messages[0];

        $view = View::make('mail.financial_order_confirmation', [
            'content'        => $message,
        ]);

        return $view->render();
    });
});
