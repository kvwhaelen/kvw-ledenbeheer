<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'store'], function () {
        Route::post('leader', 'Api\LeaderController@store');
        Route::post('child', 'Api\ChildController@store');
        Route::post('preschooler', 'Api\PreschoolerController@store');
    });

    Route::group(['prefix' => 'mollie/webhooks'], function () {
        // NEEDS TO BE REFACTORED AND REMOVED.
        Route::post('child', 'Api\MollieChildWebhookController@handle');
        Route::post('preschooler', 'Api\MolliePreschoolerWebhookController@handle');

        Route::post('payment', 'Api\Mollie\PaymentWebhookController@handle');
    });

    Route::group(['prefix' => 'financial-action-order'], function () {
        Route::post('create', 'Api\FinancialActionOrderController@store');
    });
});

Route::group(['prefix' => 'internal'], function () {

    Route::group(['prefix' => 'datatables'], function () {
        Route::post('users', 'Api\Internal\DataTables\UserController@index');
    });

});
