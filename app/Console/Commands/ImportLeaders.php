<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Excel;

class ImportLeaders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:leaders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import CSV with leaders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Excel::load('app/storage/imports/leidingopgave.xls', function ($reader) {
            dd($reader);
        });
    }
}
