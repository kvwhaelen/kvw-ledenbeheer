<?php

namespace App\Console\Commands;

use App\Models\Child;
use App\Models\Preschooler;
use Illuminate\Console\Command;
use Mollie\Api\Exceptions\ApiException;

class ReSyncPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resync:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re-sync application payments.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $thisYear = date('Y-01-01');

        $prechoolers = Preschooler::where('created_at', '>=', $thisYear)->get();
        $children = Child::where('created_at', '>=', $thisYear)->get();

        foreach ($children as $child) {
            $this->sync($child);
        }

        foreach ($prechoolers as $prechooler) {
            $this->sync($prechooler);
        }
    }

    /**
     * Check if entity is paid and update the payment status.
     *
     * @param $entity
     */
    private function sync($entity)
    {
        if (substr($entity->payment_reference, 0, 3) === 'tr_') {
            try {
                $payment = mollie()->payments()->get($entity->payment_reference);

                if ($payment->isPaid()) {
                    $entity->update([ 'payment' => true ]);
                } else {
                    $entity->update([ 'payment' => false ]);
                }
            } catch (ApiException $e) {
                echo $e->getMessage();
            }
        }
    }
}
