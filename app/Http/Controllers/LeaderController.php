<?php

namespace App\Http\Controllers;

use App\Models\Leader;
use App\Models\Member;
use App\Http\Requests\StoreLeaderApplication;

class LeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leaders = Leader::whereYear('created_at', date('Y'))
            ->get();

        return view('leaders.index', compact('leaders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leaders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLeaderApplication $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLeaderApplication $request)
    {
        $member = new Member();
        $member = $member->add($request);

        Leader::create([
            'member_id'                 => $member->id,
            'first_group_preference'    => $request->get('first_group_preference'),
            'second_group_preference'   => $request->get('second_group_preference'),
            'third_group_preference'    => $request->get('third_group_preference'),
            'co-leaders_preference'     => $request->get('co-leaders_preference'),
            'comments'                  => $request->get('comments'),
            'kvw_group'                 => $request->get('kvw_group'),
        ]);

        return back()->with('success', 'Leider succesvol toegevoegd!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Leader $leader
     * @return \Illuminate\Http\Response
     */
    public function edit(Leader $leader)
    {
        return view('leaders.edit', compact('leader'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Leader $leader
     * @param StoreLeaderApplication $request
     * @return \Illuminate\Http\Response
     */
    public function update(Leader $leader, StoreLeaderApplication $request)
    {
        $leader->fill($request->all())->save();
        $leader->member->fill($request->all())->save();

        return redirect()->route('leader.index');
    }
}
