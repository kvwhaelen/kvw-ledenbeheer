<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreFinancialActionOrder;
use App\Models\FinancialAction;
use App\Models\FinancialActionOrder;
use App\Models\FinancialActionOrderLine;
use App\Models\FinancialActionProduct;
use Illuminate\Http\JsonResponse;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Laravel\Facades\Mollie;

class FinancialActionOrderController extends Controller
{
    /**
     * Create a new financial action order.
     *
     * @param  StoreFinancialActionOrder  $request
     * @return JsonResponse
     * @throws ApiException
     */
    public function store(StoreFinancialActionOrder $request)
    {
        $financialAction = FinancialAction::find(
            $request->input('financial_action_order.financial_action_id')
        );

        $financialActionOrder = FinancialActionOrder::create([
            'financial_action_id' => $financialAction->id,
            'content' => $request->input('financial_action_order.content')
        ]);

        $lines = $request->input('financial_action_order.order_lines');
        $paymentAmount = 0;

        foreach ($lines as $line) {
            $financialActionProduct = FinancialActionProduct::find($line['product_id']);

            $financialActionOrderLine = FinancialActionOrderLine::create([
                'financial_action_order_id' => $financialActionOrder->id,
                'financial_action_product_id' => $financialActionProduct->id,
                'description' => $financialActionProduct->name,
                'unit_price' => $financialActionProduct->unit_price,
                'quantity' => $line['quantity'],
            ]);

            $paymentAmount = $paymentAmount + (
                $financialActionOrderLine->unit_price * $financialActionOrderLine->quantity
            );
        }

        $molliePayment = Mollie::api()->payments()->create([
            'amount' => [
                'currency' => 'EUR',
                'value' => number_format($paymentAmount, 2, '.', ''),
            ],
            'description' => $financialAction->name,
            'webhookUrl' => 'https://ledenbeheer.kvwhaelen.nl/api/v1/mollie/webhooks/payment',
            'redirectUrl' => $financialAction->referral_url,
        ]);


        $financialActionOrder->payments()->create([
            'payment_type_id' => config('payments.types.iDEAL'),
            'reference' => $molliePayment->id,
            'amount' => $paymentAmount,
            'is_paid' => false
        ]);

        return response()->json([
            'status' => 'success',
            'payment_url' => $molliePayment->getCheckoutUrl()
        ]);
    }
}
