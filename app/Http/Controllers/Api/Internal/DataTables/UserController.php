<?php

namespace App\Http\Controllers\Api\Internal\DataTables;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return DataTables
     * @throws \Exception
     */
    public function index()
    {
        $users = DB::table('users')
            ->select(
                'users.id AS user_id',
                'users.first_name AS user_first_name',
                'users.last_name_prefix AS user_last_name_prefix',
                'users.last_name AS user_last_name',
                'users.email AS user_email',
                'users.created_at AS user_created_at'
            );

        return datatables()->of($users)
            ->editColumn('user_created_at', function ($user) {
                return Carbon::parse($user->user_created_at)->format('d-m-Y');
            })
            ->filterColumn('user_id', function ($query, $keyword) {
                $query->where('users.id', 'like', "%$keyword%");
            })
            ->filterColumn('user_name', function ($query, $keyword) {
                $query->where('users.first_name', 'like', "%$keyword%")
                    ->orWhere('users.last_name_prefix', 'like', "%$keyword%")
                    ->orWhere('users.last_name', 'like', "%$keyword%");
            })
            ->filterColumn('user_email', function ($query, $keyword) {
                $query->where('users.email', 'like', "%$keyword%");
            })
            ->toJson();
    }
}
