<?php

namespace App\Http\Controllers\Api;

use App\Mail\FinancialActionConfirmation;
use App\Mail\LeaderApplicationConfirmation;
use App\Models\Leader;
use App\Models\Member;
use App\Http\Requests\Api\StoreLeaderApplication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class LeaderController extends Controller
{

    /**
     * Store new Leader application.
     *
     * @param StoreLeaderApplication $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreLeaderApplication $request)
    {
        $member = new Member();
        $member = $member->add($request);

        $leader = Leader::create([
            'member_id'                 => $member->id,
            'first_group_preference'    => $request->get('first_group_preference'),
            'second_group_preference'   => $request->get('second_group_preference'),
            'third_group_preference'    => $request->get('third_group_preference'),
            'co-leaders_preference'     => $request->get('co-leaders_preference'),
            'comments'                  => $request->get('comments'),
        ]);

        Mail::to($leader->member->email)
            ->send(
                new LeaderApplicationConfirmation($leader)
            );

        return response()->json([
            'status' => 'success'
        ]);
    }
}
