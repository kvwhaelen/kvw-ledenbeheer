<?php

namespace App\Http\Controllers\Api;

use App\Models\Child;
use App\Models\Member;
use App\Http\Requests\Api\StoreChildApplication;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Laravel\Facades\Mollie;

class ChildController extends Controller
{
    /**
     * Store new child application.
     *
     * @param  StoreChildApplication  $request
     * @return JsonResponse
     * @throws ApiException
     */
    public function store(StoreChildApplication $request)
    {
        $member = new Member();
        $member = $member->add($request);

        $molliePayment = Mollie::api()->payments()->create([
            'amount' => [
                'currency' => 'EUR',
                'value' => '27.50',
            ],
            'description' => 'Opgave KVW-week',
            'webhookUrl' => 'https://ledenbeheer.kvwhaelen.nl/api/v1/mollie/webhooks/payment',
            'redirectUrl' => 'https://kvwhaelen.nl/doe-mee/kinderen?success',
        ]);

        $child = Child::create([
            'member_id'                => $member->id,
            'school'                   => $request->get('school'),
            'school_group'             => $request->get('school_group'),
            'first_friend_preference'  => $request->get('first_friend_preference'),
            'second_friend_preference' => $request->get('second_friend_preference'),
            'comments'                 => $request->get('comments'),
        ]);

        $child->payments()->create([
            'payment_type_id' => config('payments.types.iDEAL'),
            'reference' => $molliePayment->id,
            'amount' => $molliePayment->amount->value,
            'is_paid' => false
        ]);

        return response()->json([
            'status'      => 'success',
            'payment_url' => $molliePayment->getCheckoutUrl()
        ]);
    }
}
