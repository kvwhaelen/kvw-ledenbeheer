<?php

namespace App\Http\Controllers\Api;

use App\Models\Preschooler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mollie\Laravel\Facades\Mollie;

class MolliePreschoolerWebhookController extends Controller
{
    public function handle(Request $request) {
        if (!$request->has('id')) {
            abort(400, 'Request should have an ID');
        }

        try {
            $payment = Mollie::api()->payments()->get($request->id);

            if($payment->isPaid()) {
                Preschooler::where('payment_reference', $payment->id)
                    ->first()
                    ->update([
                        'payment' => true
                    ]);
            }
        } catch (\Mollie_API_Exception $e) {
            abort(400, 'No payment found');
        }
    }
}
