<?php

namespace App\Http\Controllers\Api\Mollie;

use App\Events\PaymentPayed;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;

class PaymentWebhookController extends Controller
{
    public function handle(Request $request) {
        if (!$request->has('id')) {
            abort(400, 'Request should have an ID');
        }

        try {
            $molliePayment = Mollie::api()->payments()->get($request->get('id'));

            if($molliePayment->isPaid()) {
                $payment = Payment::where('reference', $molliePayment->id)
                    ->first();

                $payment->update([
                    'is_paid' => true
                ]);

                PaymentPayed::dispatch($payment);
            }
        } catch (\Mollie_API_Exception $e) {
            abort(400, 'No payment found');
        }
    }
}
