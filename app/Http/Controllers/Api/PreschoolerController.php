<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\StorePreschoolerApplication;
use App\Models\Member;
use App\Models\Preschooler;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Laravel\Facades\Mollie;

class PreschoolerController extends Controller
{
    /**
     * Store new Preschooler application
     *
     * @param  StorePreschoolerApplication $request
     *
     * @return JsonResponse
     * @throws ApiException
     */
    public function store(StorePreschoolerApplication $request)
    {
        $member = new Member();
        $member = $member->add($request);

        $molliePayment = Mollie::api()->payments()->create([
            'amount' => [
                'currency' => 'EUR',
                'value' => '7.50',
            ],
            'description' => 'Opgave KVW kleuterdag',
            'webhookUrl' => 'https://ledenbeheer.kvwhaelen.nl/api/v1/mollie/webhooks/payment',
            'redirectUrl' => 'https://kvwhaelen.nl/doe-mee/kleuters?success',
        ]);

        $preschooler = Preschooler::create([
            'member_id'                 => $member->id,
            'school'                    => $request->get('school'),
            'school_group'              => $request->get('school_group'),
            'first_friend_preference'   => $request->get('first_friend_preference'),
            'second_friend_preference'  => $request->get('second_friend_preference'),
            'comments'                  => $request->get('comments'),
        ]);

        $preschooler->payments()->create([
            'payment_type_id' => config('payments.types.iDEAL'),
            'reference' => $molliePayment->id,
            'amount' => $molliePayment->amount->value,
            'is_paid' => false
        ]);

        return response()->json([
            'status'        => 'success',
            'payment_url'   => $molliePayment->getCheckoutUrl()
        ]);
    }
}
