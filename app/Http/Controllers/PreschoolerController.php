<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePreschoolerApplication;
use App\Models\Member;
use App\Models\Preschooler;

class PreschoolerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preschoolers = Preschooler::whereYear('created_at', date('Y'))
            ->with('payments')
            ->whereHas('payments', function($query)  {
                return $query->where('is_paid', true);
            })
            ->get();

        return view('preschooler.index', compact('preschoolers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Preschooler $preschooler
     * @return \Illuminate\Http\Response
     */
    public function edit(Preschooler $preschooler)
    {
        return view('preschooler.edit', compact('preschooler'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('preschooler.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePreschoolerApplication $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePreschoolerApplication $request)
    {
        $member = new Member();
        $member = $member->add($request);

        $preschooler = Preschooler::create([
            'member_id'                 => $member->id,
            'school'                    => $request->get('school'),
            'school_group'              => $request->get('school_group'),
            'first_friend_preference'   => $request->get('first_friend_preference'),
            'second_friend_preference'  => $request->get('second_friend_preference'),
            'comments'                  => $request->get('comments'),
            'payment'                   => true,
            'payment_reference'         => 'PAPER',
        ]);

        $preschooler->payments()->create([
            'payment_type_id' => config('payments.types.unkown'),
            'reference' => 'Handmatig',
            'amount' => 5.00,
            'is_paid' => true
        ]);

        return back()->with('success', 'Kleuter succesvol toegevoegd!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Preschooler $preschooler
     * @param StorePreschoolerApplication $request
     * @return \Illuminate\Http\Response
     */
    public function update(Preschooler $preschooler, StorePreschoolerApplication $request)
    {
        $preschooler->fill($request->all())->save();
        $preschooler->member->fill($request->all())->save();

        return back();
    }
}
