<?php

namespace App\Http\Controllers;

use App\Models\Child;
use App\Models\Member;
use App\Http\Requests\StoreChildApplication;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $children = Child::whereYear('created_at', date('Y'))
            ->with('payments')
            ->whereHas('payments', function($query)  {
                return $query->where('is_paid', true);
            })
            ->get();

        return view('children.index', compact('children'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('children.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChildApplication $request
     * @return RedirectResponse
     */
    public function store(StoreChildApplication $request)
    {
        $member = new Member();
        $member = $member->add($request);

        $child = Child::create([
            'member_id'                 => $member->id,
            'school'                    => $request->get('school'),
            'school_group'              => $request->get('school_group'),
            'first_friend_preference'   => $request->get('first_friend_preference'),
            'second_friend_preference'  => $request->get('second_friend_preference'),
            'comments'                  => $request->get('comments'),
            'kvw_group'                 => $request->get('kvw_group'),
            'payment'                   => true,
            'payment_reference'         => 'PAPER',
        ]);

        $child->payments()->create([
            'payment_type_id' => config('payments.types.unkown'),
            'reference' => 'Handmatig',
            'amount' => 18.00,
            'is_paid' => true
        ]);

        return back()->with('success', 'Kind succesvol toegevoegd!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Child $child
     * @return View
     */
    public function edit(Child $child)
    {
        return view('children.edit', compact('child'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Child $child
     * @param StoreChildApplication $request
     * @return RedirectResponse
     */
    public function update(Child $child, StoreChildApplication $request)
    {
        $child->fill($request->all())->save();
        $child->member->fill($request->all())->save();

        return redirect()->route('child.index');
    }
}
