<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePreschoolerApplication extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'       => ['required'],
            'lastname'        => ['required'],
            'street'          => ['required'],
            'house_number'    => ['required'],
            'zipcode'         => ['required'],
            'city'            => ['required'],
            'gender'          => ['required'],
            'birthday'        => ['required'],
            'phone_one'       => ['required'],
            'email'           => ['nullable', 'email'],
            'gp'              => ['required'],
            'gp_city'         => ['required'],
            'school'          => ['required'],
            'school_group'    => ['required'],
            'comments'        => ['nullable'],
        ];
    }
}
