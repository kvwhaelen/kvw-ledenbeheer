<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class StoreFinancialActionOrder extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'financial_action_order.financial_action_id' => ['required', 'exists:financial_actions,id'],
            'financial_action_order.order_lines' => ['required', 'array'],
            'financial_action_order.order_lines.*.product_id' => ['required', 'exists:financial_action_products,id'],
            'financial_action_order.order_lines.*.quantity' => ['required', 'numeric', 'min:1'],
            'financial_action_order.content' => ['required'],
            'financial_action_order.content.email' => ['required', 'email'],
            'financial_action_order.content.name_from' => ['required', 'string'],
            'financial_action_order.content.messages' => ['required', 'array'],
            'financial_action_order.content.messages.*.quantity' => ['required', 'numeric', 'min:1'],
            'financial_action_order.content.messages.*.name_to' => ['required', 'string'],
            'financial_action_order.content.messages.*.street' => ['required', 'string'],
            'financial_action_order.content.messages.*.number' => ['required', 'string'],
            'financial_action_order.content.messages.*.city' => ['required', 'string'],
            'financial_action_order.content.messages.*.message' => ['required', 'string'],
        ];
    }
}
