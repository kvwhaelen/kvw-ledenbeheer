<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class StoreLeaderApplication extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'     => ['required'],
            'lastname'      => ['required'],
            'street'        => ['required'],
            'house_number'  => ['required'],
            'zipcode'       => ['required'],
            'city'          => ['required'],
            'gender'        => ['required'],
            'birthday'      => ['required'],
            'phone_one'     => ['required'],
            'email'         => ['required', 'email'],
            'first_group_preference'  => ['required'],
            'second_group_preference' => ['required'],
            'third_group_preference'  => ['required'],
            'comments'      => ['nullable'],
        ];
    }
}
