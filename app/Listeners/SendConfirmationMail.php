<?php

namespace App\Listeners;

use App\Events\PaymentPayed;
use App\Mail\ChildApplicationConfirmation;
use App\Mail\FinancialActionConfirmation;
use App\Mail\PreschoolerApplicationConfirmation;
use App\Models\Child;
use App\Models\FinancialActionOrder;
use App\Models\Preschooler;
use Illuminate\Support\Facades\Mail;

class SendConfirmationMail
{
    /**
     * Handle the event.
     *
     * @param  PaymentPayed  $event
     * @return void
     */
    public function handle(PaymentPayed $event)
    {
        if ($event->payment->payable instanceof FinancialActionOrder) {
            Mail::to($event->payment->payable->content->email)
                ->send(
                    new FinancialActionConfirmation($event->payment->payable)
                );
        }

        elseif ($event->payment->payable instanceof Child) {
            Mail::to($event->payment->payable->member->email)
                ->send(
                    new ChildApplicationConfirmation($event->payment->payable)
                );
        }

        elseif ($event->payment->payable instanceof Preschooler) {
            Mail::to($event->payment->payable->member->email)
                ->send(
                    new PreschoolerApplicationConfirmation($event->payment->payable)
                );
        }
    }
}
