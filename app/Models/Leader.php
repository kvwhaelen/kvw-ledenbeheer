<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Leader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leader_applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'first_group_preference',
        'second_group_preference',
        'third_group_preference',
        'co-leaders_preference',
        'comments',
        'kvw_group'
    ];

    public function add(Request $request, $member_id)
    {
        $leader = new leader(['member_id' => $member_id] + $request->all());
        $leader->save();
    }

    /**
     * Get the member that belongs to the leader.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }

    /**
     * Set the leaders first group preference.
     *
     * @param  $value
     * @return void
     */
    public function setFirstGroupPreferenceAttribute($value)
    {
        $this->attributes['first_group_preference'] = $value == '0' ? null : $value;
    }

    /**
     * Set the leaders second group preference.
     *
     * @param  $value
     * @return void
     */
    public function setSecondGroupPreferenceAttribute($value)
    {
        $this->attributes['second_group_preference'] = $value == '0' ? null : $value;
    }

    /**
     * Set the leaders third group preference.
     *
     * @param  $value
     * @return void
     */
    public function setThirdGroupPreferenceAttribute($value)
    {
        $this->attributes['third_group_preference'] = $value == '0' ? null : $value;
    }
}
