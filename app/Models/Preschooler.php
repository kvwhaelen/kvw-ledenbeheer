<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Http\Request;

class Preschooler extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'preschooler_applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'school',
        'school_group',
        'first_friend_preference',
        'second_friend_preference',
        'comments',
    ];

    /**
     * Get the member that belongs to the preschooler.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }

    /**
     * A financial action order can have many payments.
     *
     * @return MorphMany
     */
    public function payments(): MorphMany
    {
        return $this->morphMany(Payment::class, 'payable');
    }
}
