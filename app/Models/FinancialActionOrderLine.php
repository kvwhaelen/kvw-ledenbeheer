<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FinancialActionOrderLine extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'financial_action_order_id',
        'financial_action_product_id',
        'description',
        'unit_price',
        'quantity',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'unit_price' => 'float',
        'quantity' => 'float',
    ];

    /**
     * A financial action order line belongs to a financial action order.
     *
     * @return BelongsTo
     */
    public function financialActionOrder(): BelongsTo
    {
        return $this->belongsTo(FinancialActionOrder::class);
    }

    /**
     * A financial action order line belongs to a financial action product.
     *
     * @return BelongsTo
     */
    public function financialActionProduct(): BelongsTo
    {
        return $this->belongsTo(FinancialActionProduct::class);
    }
}
