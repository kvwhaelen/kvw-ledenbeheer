<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FinancialActionProduct extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'financial_action_id',
        'name',
        'unit_price',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'unit_price' => 'float',
    ];

    /**
     * A financial action product belongs to a financial action.
     *
     * @return BelongsTo
     */
    public function financialAction(): BelongsTo
    {
        return $this->belongsTo(FinancialAction::class);
    }

    /**
     *  A financial action product can have many financial action order lines.
     *
     * @return HasMany
     */
    public function financialActionOrderLines(): HasMany
    {
        return $this->hasMany(FinancialActionOrderLine::class);
    }
}
