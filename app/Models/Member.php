<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Member extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uin',
        'firstname',
        'lastname_prefix',
        'lastname',
        'street',
        'house_number',
        'zipcode',
        'city',
        'street_second',
        'house_number_second',
        'zipcode_second',
        'city_second',
        'phone_one',
        'phone_two',
        'phone_three',
        'phone_one_description',
        'phone_two_description',
        'phone_three_description',
        'email',
        'gender',
        'birthday',
        'gp',
        'gp_city',
        'shirt_size'
    ];

    /**
     * Add new member or update member with request input if member already exists.
     *
     * @param   Request $request
     * @return  string
     */
    public function add(Request $request)
    {
        $UIN = $this->generateUIN($request);

        if (Member::where('uin', '=', $UIN)->exists()) {
            $member = Member::where('uin', '=', $UIN)->first();
            $member->update($request->all());
        } else {
            $member = new Member(['uin' => $UIN] + $request->all());
            $member->save();
        }

        return $member;
    }

    /**
     * Generate a unique ID based on personal data.
     *
     * @param   Request $request
     * @return  string
     */
    private function generateUIN(Request $request)
    {
        $filtered_request = $request->only(
            [
                'gender',
                'firstname',
                'lastname_prefix',
                'lastname',
                'birthday'
            ]
        );

        $str = implode($filtered_request);
        $str = strtoupper($str);

        return md5($str);
    }
}
