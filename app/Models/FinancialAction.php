<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FinancialAction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'referral_url',
        'has_online_payment',
        'is_active',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'has_online_payment' => 'boolean',
        'is_active' => 'boolean',
    ];

    /**
     * A financial action can have many financial action orders.
     *
     * @return HasMany
     */
    public function financialActionOrders(): HasMany
    {
        return $this->hasMany(FinancialActionOrder::class);
    }

    /**
     * A financial action can have many financial action products.
     *
     * @return HasMany
     */
    public function financialActionProducts(): HasMany
    {
        return $this->hasMany(FinancialActionProduct::class);
    }
}
