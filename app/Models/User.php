<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name_prefix',
        'last_name',
        'password',
        'email',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the full name of the user
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $last_name = (!empty($this->last_name_prefix) || !isset($this->last_name_prefix)
            ? $this->last_name_prefix . ' ' . $this->last_name
            : $this->last_name);

        return $this->attributes['full_name'] = $this->first_name . ' ' . $last_name;
    }

    /**
     * Hash the password attribute before setting it
     *
     * @param $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::make($value);
    }
}
