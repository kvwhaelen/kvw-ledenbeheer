<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class FinancialActionOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'financial_action_id',
        'content',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'object',
    ];

    /**
     * A financial action order belongs to a financial action.
     *
     * @return BelongsTo
     */
    public function financialAction(): BelongsTo
    {
        return $this->belongsTo(FinancialAction::class);
    }

    /**
     * A financial action order can have many financial action order lines.
     *
     * @return HasMany
     */
    public function financialActionOrderLines(): HasMany
    {
        return $this->hasMany(FinancialActionOrderLine::class);
    }

    /**
     * A financial action order can have many payments.
     *
     * @return MorphMany
     */
    public function payments(): MorphMany
    {
        return $this->morphMany(Payment::class, 'payable');
    }
}
