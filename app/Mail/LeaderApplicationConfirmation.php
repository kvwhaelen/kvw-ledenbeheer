<?php

namespace App\Mail;

use App\Models\FinancialActionOrder;
use App\Models\Leader;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LeaderApplicationConfirmation extends Mailable
{
    use SerializesModels;

    /**
     * @var Leader The leader to handle the confirmation mail for.
     */
    private $leader;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Leader $leader)
    {
        $this->leader = $leader;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.leader_application_confirmation')
            ->with([
                'leader' => $this->leader
            ])
            ->from('secretariaat@kvwhaelen.nl', 'Stichting Kindervakantiewerk Haelen en Nunhem')
            ->subject('Leidingopgave KVW 2024');
    }
}
