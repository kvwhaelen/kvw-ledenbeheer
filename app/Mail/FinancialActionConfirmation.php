<?php

namespace App\Mail;

use App\Models\FinancialActionOrder;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FinancialActionConfirmation extends Mailable
{
    use SerializesModels;

    /**
     * @var FinancialActionOrder The financal action order to handle the confirmation mail for.
     */
    private $financialActionOrder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FinancialActionOrder $financialActionOrder)
    {
        $this->financialActionOrder = $financialActionOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $content = $this->financialActionOrder->content;

        return $this->view('mail.financial_order_confirmation')
            ->with([
                'content' => $content
            ])
            ->from('secretariaat@kvwhaelen.nl', 'Stichting Kindervakantiewerk Haelen en Nunhem')
            ->subject('Rozenactie 2022');
    }
}
