<?php

namespace App\Mail;

use App\Models\Child;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChildApplicationConfirmation extends Mailable
{
    use SerializesModels;

    /**
     * @var Child The child to handle the confirmation mail for.
     */
    private $child;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Child $child)
    {
        $this->child = $child;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.child_application_confirmation')
            ->with([
                'child' => $this->child
            ])
            ->from('secretariaat@kvwhaelen.nl', 'Stichting Kindervakantiewerk Haelen en Nunhem')
            ->subject('Opgave KVW 2024');
    }
}
