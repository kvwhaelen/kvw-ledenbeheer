<?php

namespace App\Mail;

use App\Models\Preschooler;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PreschoolerApplicationConfirmation extends Mailable
{
    use SerializesModels;

    /**
     * @var Preschooler The preschooler to handle the confirmation mail for.
     */
    private $preschooler;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Preschooler $preschooler)
    {
        $this->preschooler = $preschooler;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.preschooler_application_confirmation')
            ->with([
                'preschooler' => $this->preschooler
            ])
            ->from('secretariaat@kvwhaelen.nl', 'Stichting Kindervakantiewerk Haelen en Nunhem')
            ->subject('Kleuterdag KVW 2024');
    }
}
