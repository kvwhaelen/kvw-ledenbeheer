<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Jules Peeters',
                'email' => 'jules@kvwhaelen.nl',
                'password' => bcrypt('kvwX8pUkD'),
            ],
            [
                'name' => 'Anke Beurskens',
                'email' => 'anke@kvwhaelen.nl',
                'password' => bcrypt('kvwNz28OG'),
            ],
            [
                'name' => 'Lobke Boots',
                'email' => 'lobke@kvwhaelen.nl',
                'password' => bcrypt('kvwCt1WNF'),
            ],
        ];

        foreach ($users as $user) {
            User::firstOrCreate(
                ['email' => $user['email']],
                [
                    'name' => $user['name'],
                    'password' => $user['password'],
                ]
            );
        }
    }
}
