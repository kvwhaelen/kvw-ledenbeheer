<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialActionOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_action_order_lines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('financial_action_order_id');
            $table->foreign('financial_action_order_id')->references('id')->on('financial_action_orders');
            $table->unsignedBigInteger('financial_action_product_id');
            $table->foreign('financial_action_product_id')->references('id')->on('financial_action_products');
            $table->string('description');
            $table->decimal('unit_price', 19, 4);
            $table->decimal('quantity', 19, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('financial_action_order_lines', function (Blueprint $table) {
            $table->dropForeign(['financial_action_order_id']);
            $table->dropForeign(['financial_action_product_id']);
        });

        Schema::dropIfExists('financial_action_order_lines');
    }
}
