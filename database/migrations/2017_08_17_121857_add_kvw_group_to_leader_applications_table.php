<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKVWGroupToLeaderApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leader_applications', function($table)
        {
            $table->integer('kvw_group')->unsigned()->nullable()->after('co-leaders_preference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leader_applications', function (Blueprint $table) {
            $table->dropColumn('kvw_group');
        });
    }
}
