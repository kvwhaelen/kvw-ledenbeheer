<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->char('uin', 32);
            $table->string('firstname', 35);
            $table->string('lastname_prefix', 35)->nullable();
            $table->string('lastname', 35);
            $table->string('street', 35);
            $table->string('house_number', 10);
            $table->char('zipcode', 6);
            $table->string('city', 35);
            $table->string('street_second', 35)->nullable();
            $table->string('house_number_second', 10)->nullable();
            $table->char('zipcode_second', 6)->nullable();
            $table->string('city_second', 35)->nullable();
            $table->string('phone_one', 10)->nullable();
            $table->string('phone_two', 10)->nullable();
            $table->string('phone_three', 10)->nullable();
            $table->string('phone_one_description', 105)->nullable();
            $table->string('phone_two_description', 105)->nullable();
            $table->string('phone_three_description', 105)->nullable();
            $table->string('email', 320)->nullable();
            $table->char('gender', 1);
            $table->date('birthday');
            $table->string('gp', 105)->nullable();
            $table->string('gp_city', 35)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
