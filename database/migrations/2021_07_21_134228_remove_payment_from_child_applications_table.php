<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePaymentFromChildApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('child_applications', function (Blueprint $table) {
            $table->dropColumn('payment');
            $table->dropColumn('payment_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('child_applications', function (Blueprint $table) {
            $table->boolean('payment')->after('comments')->default(0);
            $table->string('payment_reference')->nullable()->after('payment');
        });
    }
}
