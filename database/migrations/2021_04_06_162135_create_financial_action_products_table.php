<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialActionProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_action_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('financial_action_id');
            $table->foreign('financial_action_id')->references('id')->on('financial_actions');
            $table->string('name');
            $table->decimal('unit_price', 19, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('financial_action_products', function (Blueprint $table) {
            $table->dropForeign(['financial_action_id']);
        });

        Schema::dropIfExists('financial_action_products');
    }
}
