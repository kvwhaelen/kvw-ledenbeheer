<?php

return [
    'types' => [
        'iDEAL' => ENV('PAYMENT_TYPE_ID__IDEAL'),
        'unkown' => ENV('PAYMENT_TYPE_ID__UNKOWN'),
    ]
];
