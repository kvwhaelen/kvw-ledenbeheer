@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">
        <h2>Leider toevoegen</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif

        <div class="card mt-3">
            <div class="card-body">
                <form action="{{ route('leader.store') }}" method="POST">

                    {{ csrf_field() }}

                    <div class="row">

                        <!-- Firstname -->
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input type="text" id="firstname" class="form form-control" placeholder="Voornaam" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Voornaam'" name="firstname" data-name="firstname" required>
                            </div>
                        </div>

                        <!-- Lastname -->
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input type="text" id="lastname_prefix" class="form form-control" placeholder="Tussenv." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tussenv.'" name="lastname_prefix" data-name="lastname_prefix">
                            </div>
                        </div>

                        <!-- Lastname -->
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input type="text" id="lastname" class="form form-control" placeholder="Achternaam" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Achternaam'" name="lastname" data-name="lastname" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <!-- Gender -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select class="form form-control" name="gender">
                                    <option value="">Geslacht</option>
                                    <option value="M">Man</option>
                                    <option value="V">Vrouw</option>
                                </select>
                            </div>
                        </div>

                        <!-- Birthday -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="birthday" class="form form-control" placeholder="Geboortedatum" onfocus="this.type='date', this.placeholder = ''" onblur="this.placeholder = 'Geboortedatum'" name="birthday" data-name="birthday" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- Street -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" id="street" class="form form-control" placeholder="Straat" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Straat'" name="street" data-name="street" required>
                            </div>
                        </div>

                        <!-- Housenumber -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" id="house_number" class="form form-control" placeholder="Huisnummer" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Huisnummer'" name="house_number" data-name="house_number" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- Zipcode -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" id="zipcode" class="form form-control" placeholder="Postcode" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Postcode'" name="zipcode" data-name="zipcode" maxlength="6" required>
                            </div>
                        </div>

                        <!-- City -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" id="city" class="form form-control" placeholder="Woonplaats" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Woonplaats'" name="city" data-name="city" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- E-mail -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="email" id="email" class="form form-control" placeholder="E-mailadres" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mailadres'" name="email" data-name="email" required>
                            </div>
                        </div>

                        <!-- Phone -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" id="phone_one" class="form form-control" placeholder="Telefoonnummer" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Telefoonnummer'" name="phone_one" data-name="phone_one" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- First group preference -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form form-control" name="first_group_preference">
                                    <option value="">Eerste voorkeur schoolgroep</option>
                                    <option value="0">Geen voorkeur</option>
                                    <option value="3">Groep 3</option>
                                    <option value="4">Groep 4</option>
                                    <option value="5">Groep 5</option>
                                    <option value="6">Groep 6</option>
                                    <option value="7">Groep 7</option>
                                    <option value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>

                        <!-- Second group preference -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form form-control" name="second_group_preference">
                                    <option value="">Tweede voorkeur schoolgroep</option>
                                    <option value="0">Geen voorkeur</option>
                                    <option value="3">Groep 3</option>
                                    <option value="4">Groep 4</option>
                                    <option value="5">Groep 5</option>
                                    <option value="6">Groep 6</option>
                                    <option value="7">Groep 7</option>
                                    <option value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>

                        <!-- Third group preference -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form form-control" name="third_group_preference">
                                    <option value="">Derde voorkeur schoolgroep</option>
                                    <option value="0">Geen voorkeur</option>
                                    <option value="3">Groep 3</option>
                                    <option value="4">Groep 4</option>
                                    <option value="5">Groep 5</option>
                                    <option value="6">Groep 6</option>
                                    <option value="7">Groep 7</option>
                                    <option value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <textarea name="co-leaders_preference" class="form-control" placeholder="Voorkeur medeleiding"></textarea>
                    </div>

                    <div class="form-group">
                        <textarea name="comments" class="form-control" placeholder="Bijzonderheden"></textarea>
                    </div>

                    <!-- Button submit -->
                    <button id="valid-form" class="btn btn-primary">Opgeven!</button>

                </form>
            </div>
        </div>
    </div>
@endsection