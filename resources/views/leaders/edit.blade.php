@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">
        <h2>Leider aanpassen</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (!empty($success))
            <div class="alert alert-success">
                <ul>
                    {{ $success }}
                </ul>
            </div>
        @endif

        <div class="card mt-3">
            <div class="card-body">
                <form action="{{ route('leader.update', ['leader' => $leader->id]) }}" method="POST">

                    {{ csrf_field() }}

                    <div class="row">

                        <!-- Firstname -->
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input id="firstname" class="form-control" placeholder="Voornaam" name="firstname" value="{{ $leader->member->firstname }}" required>
                            </div>
                        </div>

                        <!-- Lastname prefix -->
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input id="lastname_prefix" class="form-control" placeholder="Tussenv." name="lastname_prefix" value="{{ $leader->member->lastname_prefix }}">
                            </div>
                        </div>

                        <!-- Lastname -->
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input id="lastname" class="form-control" placeholder="Achternaam" name="lastname" value="{{ $leader->member->lastname }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <!-- Gender -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select class="form-control" name="gender">
                                    <option @if($leader->member->gender == 'M') selected @endif value="M">Man</option>
                                    <option @if($leader->member->gender == 'V') selected @endif value="V">Vrouw</option>
                                </select>
                            </div>
                        </div>

                        <!-- Birthday -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="birthday" type="date" class="form-control" placeholder="Geboortedatum" name="birthday" value="{{ $leader->member->birthday }}" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- Street -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="street" class="form-control" placeholder="Straat" name="street" value="{{ $leader->member->street }}" required>
                            </div>
                        </div>

                        <!-- House number -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="house_number" class="form-control" placeholder="Huisnummer" name="house_number" value="{{ $leader->member->house_number }}" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- Zipcode -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="zipcode" class="form-control" placeholder="Postcode" name="zipcode"maxlength="6" value="{{ $leader->member->zipcode }}" required>
                            </div>
                        </div>

                        <!-- City -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="city" class="form-control" placeholder="Woonplaats" name="city" value="{{ $leader->member->city }}" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- E-mail -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="email" id="email" class="form-control" placeholder="E-mailadres" name="email" value="{{ $leader->member->email }}" required>
                            </div>
                        </div>

                        <!-- Phone -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input id="phone_one" class="form-control" placeholder="Telefoonnummer" name="phone_one" value="{{ $leader->member->phone_one }}" required>
                            </div>
                        </div>

                        <!-- KVW group -->
                        <div class="col-md-4">
                            <div class="form-group field field-text">
                                <input name="kvw_group" id="kvw_group" class="form-control" placeholder="KVW groep" type="number" value="{{ $leader->kvw_group }}">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <!-- First group preference -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form form-control" name="first_group_preference">
                                    <option value="">Eerste voorkeur schoolgroep</option>
                                    <option @if($leader->first_group_preference == '0') selected @endif value="0">Geen voorkeur</option>
                                    <option @if($leader->first_group_preference == '3') selected @endif value="3">Groep 3</option>
                                    <option @if($leader->first_group_preference == '4') selected @endif value="4">Groep 4</option>
                                    <option @if($leader->first_group_preference == '5') selected @endif value="5">Groep 5</option>
                                    <option @if($leader->first_group_preference == '6') selected @endif value="6">Groep 6</option>
                                    <option @if($leader->first_group_preference == '7') selected @endif value="7">Groep 7</option>
                                    <option @if($leader->first_group_preference == '8') selected @endif value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>

                        <!-- Second group preference -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form form-control" name="second_group_preference">
                                    <option value="">Tweede voorkeur schoolgroep</option>
                                    <option @if($leader->second_group_preference == '0') selected @endif value="0">Geen voorkeur</option>
                                    <option @if($leader->second_group_preference == '3') selected @endif value="3">Groep 3</option>
                                    <option @if($leader->second_group_preference == '4') selected @endif value="4">Groep 4</option>
                                    <option @if($leader->second_group_preference == '5') selected @endif value="5">Groep 5</option>
                                    <option @if($leader->second_group_preference == '6') selected @endif value="6">Groep 6</option>
                                    <option @if($leader->second_group_preference == '7') selected @endif value="7">Groep 7</option>
                                    <option @if($leader->second_group_preference == '8') selected @endif value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>

                        <!-- Third group preference -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form form-control" name="third_group_preference">
                                    <option value="">Derde voorkeur schoolgroep</option>
                                    <option @if($leader->third_group_preference == '0') selected @endif value="0">Geen voorkeur</option>
                                    <option @if($leader->third_group_preference == '3') selected @endif value="3">Groep 3</option>
                                    <option @if($leader->third_group_preference == '4') selected @endif value="4">Groep 4</option>
                                    <option @if($leader->third_group_preference == '5') selected @endif value="5">Groep 5</option>
                                    <option @if($leader->third_group_preference == '6') selected @endif value="6">Groep 6</option>
                                    <option @if($leader->third_group_preference == '7') selected @endif value="7">Groep 7</option>
                                    <option @if($leader->third_group_preference == '8') selected @endif value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea name="co-leaders_preference" class="form-control" placeholder="Voorkeur medeleiding">{{ $leader->{'co-leaders_preference'} }}</textarea>
                    </div>

                    <div class="form-group">
                        <textarea name="comments" class="form-control" placeholder="Bijzonderheden">{{ $leader->comments }}</textarea>
                    </div>

                    <!-- Button submit -->
                    <button id="valid-form" class="btn btn-primary">Opslaan</button>

                </form>
            </div>
        </div>
    </div>

@endsection
