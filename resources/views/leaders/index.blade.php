@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Leiding overzicht</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('leader.create') }}" class="btn btn-outline-primary">
                    <i class="fas fa-plus"></i> Toevoegen
                </a>
            </div>
        </div>

        <div class="table-responsive mt-4">
            <table id="leaders" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Geslacht</th>
                        <th>Voornaam</th>
                        <th>Tussenvoegsel</th>
                        <th>Achternaam</th>
                        <th>Geboortedatum</th>
                        <th>Leeftijd</th>
                        <th>V1</th>
                        <th>V2</th>
                        <th>V3</th>
                        <th>E-mailadres</th>
                        <th>Medeleiding</th>
                        <th>Straat</th>
                        <th>Huisnummer</th>
                        <th>Postcode</th>
                        <th>Plaatsnaam</th>
                        <th>Telefoon</th>
                        <th>KVW Groep</th>
                        <th>Opmerkingen</th>
                        <th class="dontprint"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($leaders as $leader)
                        <tr>
                            <td>{{ $leader->id }}</td>
                            <td>{{ $leader->member->gender }}</td>
                            <td>{{ $leader->member->firstname }}</td>
                            <td>{{ $leader->member->lastname_prefix }}</td>
                            <td>{{ $leader->member->lastname }}</td>
                            <td>{{ $leader->member->birthday }}</td>
                            <td>{{ \Carbon\Carbon::parse($leader->member->birthday)->age }}</td>
                            <td>{{ $leader->first_group_preference }}</td>
                            <td>{{ $leader->second_group_preference }}</td>
                            <td>{{ $leader->third_group_preference }}</td>
                            <td>{{ $leader->member->email }}</td>
                            <td>{{ $leader->{'co-leaders_preference'} }}</td>
                            <td>{{ $leader->member->street }}</td>
                            <td>{{ $leader->member->house_number }}</td>
                            <td>{{ $leader->member->zipcode }}</td>
                            <td>{{ $leader->member->city }}</td>
                            <td>{{ $leader->member->phone_one }}</td>
                            <td>{{ $leader->kvw_group }}</td>
                            <td>{{ $leader->comments }}</td>
                            <td class="dontprint">
                                <a href="{{ route('leader.edit', ['leader' => $leader->id]) }}"><i class="fas fa-pencil-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('table#leaders').DataTable({
                stateSave: true,
                dom: 'Bfirtlp',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible',
                        },
                    },
                    {
                        extend: 'colvis',
                        text: 'Filter',
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
                    }
                ],
                'columnDefs': [
                    {
                        'orderable': false,
                        'targets': 16,
                    }
                ],
                'fixedColumns':   {
                    'rightColumns': 1,
                },
                'scrollX': true,
                'language': {
                    'sProcessing': 'Bezig...',
                    'sLengthMenu': '_MENU_ resultaten weergeven',
                    'sZeroRecords': 'Geen resultaten gevonden',
                    'sInfo': '_START_ tot _END_ van _TOTAL_ resultaten',
                    'sInfoEmpty': 'Geen resultaten om weer te geven',
                    'sInfoFiltered': ' (gefilterd uit _MAX_ resultaten)',
                    'sInfoPostFix': '',
                    'sSearch': 'Zoeken:',
                    'sEmptyTable': 'Geen resultaten aanwezig in de tabel',
                    'sInfoThousands': '.',
                    'sLoadingRecords': 'Een moment geduld aub - bezig met laden...',
                    'oPaginate': {
                        'sFirst': 'Eerste',
                        'sLast': 'Laatste',
                        'sNext': 'Volgende',
                        'sPrevious': 'Vorige'
                    },
                    'oAria': {
                        'sSortAscending':  ': activeer om kolom oplopend te sorteren',
                        'sSortDescending': ': activeer om kolom aflopend te sorteren'
                    }
                },
            });
        });
    </script>
@endsection
