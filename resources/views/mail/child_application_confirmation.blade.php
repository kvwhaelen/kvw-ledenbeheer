@extends('mail.layout')

@section('header_image', asset('images/mail/child_application_confirmation/header.png'))

@section('content')
    <h1>Opgave KVW 2024</h1>

    <p>Beste ouders/verzorgers van {{ $child->member->firstname }},</p>

    <p>
        Bedankt voor de opgave voor KVW 2024! We hebben de betaling in goede orde mogen ontvangen.
    </p>

    <p>Onderstaand een overzicht van de opgave:</p>

    <p>
        <b>Voornaam:</b><br>
        <i>{{ $child->member->firstname }}</i>
    </p>

    @isset($child->member->lastname_prefix)
    <p>
        <b>Tussenvoegsel:</b><br>
        <i>{{ $child->member->lastname_prefix }}</i>
    </p>
    @endisset

    <p>
        <b>Achternaam:</b><br>
        <i>{{ $child->member->lastname }}</i>
    </p>

    <p>
        <b>Adres:</b><br>
        <i>{{ $child->member->street }} {{ $child->member->house_number }}, {{ $child->member->zipcode }} {{ $child->member->city }}</i>
    </p>

    <p>
        <b>Telefoonnummer:</b><br>
        <i>{{ $child->member->phone_one }}</i>
    </p>

    <p>
        <b>Huisarts:</b><br>
        <i>{{ $child->member->gp }}</i>
    </p>

    <p>
        <b>Plaats huisarts:</b><br>
        <i>{{ $child->member->gp_city }}</i>
    </p>

    <p>
        <b>School:</b><br>
        <i>{{ $child->school }}</i>
    </p>

    <p>
        <b>Schoolgroep:</b><br>
        <i>{{ $child->school_group }}</i>
    </p>

    <p>
        <b>Ik wil in de groep bij:</b><br>
        <i>{{ $child->first_friend_preference }}, {{ $child->second_friend_preference }}</i>
    </p>

    <p>
        <b>T-shirt maat:</b><br>
        <i>{{ $child->member->shirt_size }}</i>
    </p>

    <p>
        <b>Opmerkingen:</b><br>
        @isset($child->comments)
            <i>{!! nl2br(e($child->comments)) !!}</i>
        @else
            <i>-</i>
        @endisset
    </p>

    <p>
        Voor vragen/opmerkingen zijn wij per mail bereikbaar op:<br>
        <a href="mailto:secretariaat@kvwhaelen.nl" style="color:#E83E8C;">secretariaat@kvwhaelen.nl</a>
    </p>

    <p>
        Nogmaals dank en tot snel,<br>
        Bestuur Stichting Kindervakantiewerk Haelen en Nunhem
    </p>
@endsection

@section('footer_image', asset('images/mail/child_application_confirmation/footer.png'))
