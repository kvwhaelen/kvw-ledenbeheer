@extends('mail.layout')

@section('header_image', asset('images/mail/financial_order_confirmation/header.png'))

@section('content')
    <h1>Rozenactie 2022</h1>

    <p>Beste {{ $content->name_from }},</p>

    <p>Bedankt voor het deelnemen aan de KVW-rozenactie! De bestelling is in goede orde ontvangen en we zullen deze op 7 mei (uitloop 8 mei) bezorgen.</p>
    
    <p>
        Voor vragen/opmerkingen zijn wij per mail bereikbaar op:<br>
        <a href="mailto:secretariaat@kvwhaelen.nl" style="color:#E83E8C;">secretariaat@kvwhaelen.nl</a>
    </p>
    <p>
        Nogmaals dank voor je steun,<br>
        Bestuur Stichting Kindervakantiewerk Haelen en Nunhem
    </p>
@endsection

@section('footer_image', asset('images/mail/financial_order_confirmation/footer.png'))
