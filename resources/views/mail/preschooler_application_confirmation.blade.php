@extends('mail.layout')

@section('header_image', asset('images/mail/preschooler_application_confirmation/header.png'))

@section('content')
    <h1>Opgave KVW 2024</h1>

    <p>Beste ouders/verzorgers van {{ $preschooler->member->firstname }},</p>

    <p>Bedankt voor de opgave voor de kleuterdag van KVW 2024! We hebben de betaling inmiddels in goede orde ontvangen.</p>

    <p>Onderstaand een overzicht van de opgave:</p>

    <p>
        <b>Voornaam:</b><br>
        <i>{{ $preschooler->member->firstname }}</i>
    </p>

    @isset($preschooler->member->lastname_prefix)
        <p>
            <b>Tussenvoegsel:</b><br>
            <i>{{ $preschooler->member->lastname_prefix }}</i>
        </p>
    @endisset

    <p>
        <b>Achternaam:</b><br>
        <i>{{ $preschooler->member->lastname }}</i>
    </p>

    <p>
        <b>Adres:</b><br>
        <i>{{ $preschooler->member->street }} {{ $preschooler->member->house_number }}, {{ $preschooler->member->zipcode }} {{ $preschooler->member->city }}</i>
    </p>

    <p>
        <b>Telefoonnummer:</b><br>
        <i>{{ $preschooler->member->phone_one }}</i>
    </p>

    <p>
        <b>Huisarts:</b><br>
        <i>{{ $preschooler->member->gp }}</i>
    </p>

    <p>
        <b>Plaats huisarts:</b><br>
        <i>{{ $preschooler->member->gp_city }}</i>
    </p>

    <p>
        <b>School:</b><br>
        <i>{{ $preschooler->school }}</i>
    </p>

    <p>
        <b>Schoolgroep:</b><br>
        <i>{{ $preschooler->school_group }}</i>
    </p>

    <p>
        <b>Ik wil in de groep bij:</b><br>
        <i>{{ $preschooler->first_friend_preference }}, {{ $preschooler->second_friend_preference }}</i>
    </p>

    <p>
        <b>Opmerkingen:</b><br>
        @isset($preschooler->comments)
            <i>{!! nl2br(e($preschooler->comments)) !!}</i>
        @else
            <i>-</i>
        @endisset
    </p>

    <p>
        Voor vragen/opmerkingen zijn wij per mail bereikbaar op:<br>
        <a href="mailto:secretariaat@kvwhaelen.nl" style="color:#E83E8C;">secretariaat@kvwhaelen.nl</a>
    </p>

    <p>
        Nogmaals dank en tot snel,<br>
        Bestuur Stichting Kindervakantiewerk Haelen en Nunhem
    </p>
@endsection

@section('footer_image', asset('images/mail/preschooler_application_confirmation/footer.png'))
