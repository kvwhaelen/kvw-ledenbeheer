@extends('mail.layout')

@section('header_image', asset('images/mail/leader_application_confirmation/header.png'))

@section('content')
    <h1>Leidingopgave 2024</h1>

    <p>Beste {{ $leader->member->firstname }},</p>

    <p>Bedankt voor je opgave voor de legendarische leiding van KVW 2024!</p>
    <p>
        We houden je per mail op de hoogte zodra we meer bekend kunnen maken over het programma. Schrijf alvast de voorbereidingsweek en de KVW-week in je agenda.<br><br>
        <b>Voorbereidingsweek:</b> 5 t/m 9 augustus <br>
        <b>KVW-week:</b> 12 t/m 16 augustus
    </p>

    <p>Onderstaand een overzicht van je opgave:</p>

    <p>
        <b>Adres:</b><br>
        <i>{{ $leader->member->street }} {{ $leader->member->house_number }}, {{ $leader->member->zipcode }} {{ $leader->member->city }}</i>
    </p>

    <p>
        <b>Telefoonnummer:</b><br>
        <i>{{ $leader->member->phone_one }}</i>
    </p>

    <p>
        <b>Voorkeur 1:</b><br>
        <i>
            @isset($leader->first_group_preference)
                Groep {{ $leader->first_group_preference }}
            @else
                Geen voorkeur
            @endisset
        </i>
    </p>

    <p>
        <b>Voorkeur 2:</b><br>
        <i>
            @isset($leader->second_group_preference)
                Groep {{ $leader->second_group_preference }}
            @else
                Geen voorkeur
            @endisset
        </i>
    </p>

    <p>
        <b>Voorkeur 3:</b><br>
        <i>
            @isset($leader->third_group_preference)
                Groep {{ $leader->third_group_preference }}
            @else
                Geen voorkeur
            @endisset
        </i>
    </p>

    <p>
        <b>Voorkeur medeleiding:</b><br>
        <i>{{ $leader["co-leaders_preference"] }}</i>
    </p>

    <p>
        <b>Opmerkingen:</b><br>
        <i>{!! nl2br(e($leader->comments)) !!}</i>
    </p>

    <p>
        Voor vragen/opmerkingen zijn wij per mail bereikbaar op:<br>
        <a href="mailto:secretariaat@kvwhaelen.nl" style="color:#E83E8C;">secretariaat@kvwhaelen.nl</a>
    </p>

    <p>
        Nogmaals dank en tot snel,<br>
        Bestuur Stichting Kindervakantiewerk Haelen en Nunhem
    </p>
@endsection

@section('footer_image', asset('images/mail/leader_application_confirmation/footer.png'))
