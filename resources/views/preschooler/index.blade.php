@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <h2>Kleuter overzicht</h2>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route('preschooler.create') }}" class="btn btn-outline-primary">
                    <i class="fas fa-plus"></i> Toevoegen
                </a>
            </div>
        </div>

        <div class="table-responsive mt-4">
            <table id="preschoolers" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Geslacht</th>
                        <th>Voornaam</th>
                        <th>Tussenvoegsel</th>
                        <th>Achternaam</th>
                        <th>Schoolgroep</th>
                        <th>V1</th>
                        <th>V2</th>
                        <th>E-mailadres</th>
                        <th>Telefoonnummer</th>
                        <th>Straatnaam</th>
                        <th>Huisnr.</th>
                        <th>Postcode</th>
                        <th>Plaatsnaam</th>
                        <th>Opmerkingen</th>
                        <th class="dontprint"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($preschoolers as $key=>$preschooler)
                        <tr>
                            <td>{{ $preschooler->id }}</td>
                            <td>{{ $preschooler->member->gender }}</td>
                            <td>{{ $preschooler->member->firstname }}</td>
                            <td>{{ $preschooler->member->lastname_prefix }}</td>
                            <td>{{ $preschooler->member->lastname }}</td>
                            <td>{{ $preschooler->school_group }}</td>
                            <td>{{ $preschooler->first_friend_preference }}</td>
                            <td>{{ $preschooler->second_friend_preference }}</td>
                            <td>{{ $preschooler->member->email }}</td>
                            <td>{{ $preschooler->member->phone_one }}</td>
                            <td>{{ $preschooler->member->street }}</td>
                            <td>{{ $preschooler->member->house_number }}</td>
                            <td>{{ $preschooler->member->zipcode }}</td>
                            <td>{{ $preschooler->member->city }}</td>
                            <td>{{ $preschooler->comments }}</td>

                            <td class="dontprint">
                                <a href="{{ route('preschooler.edit', ['preschooler' => $preschooler->id]) }}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('table#preschoolers').DataTable({
                stateSave: true,
                dom: 'Bfirtlp',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible',
                        },
                    },
                    {
                        extend: 'colvis',
                        text: 'Filter',
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    }
                ],
                'columnDefs': [
                    {
                        'orderable': false,
                        'targets': 11,
                    }
                ],
                'fixedColumns':   {
                    'rightColumns': 1,
                },
                'scrollX': true,
                'language': {
                    'sProcessing': 'Bezig...',
                    'sLengthMenu': '_MENU_ resultaten weergeven',
                    'sZeroRecords': 'Geen resultaten gevonden',
                    'sInfo': '_START_ tot _END_ van _TOTAL_ resultaten',
                    'sInfoEmpty': 'Geen resultaten om weer te geven',
                    'sInfoFiltered': ' (gefilterd uit _MAX_ resultaten)',
                    'sInfoPostFix': '',
                    'sSearch': 'Zoeken:',
                    'sEmptyTable': 'Geen resultaten aanwezig in de tabel',
                    'sInfoThousands': '.',
                    'sLoadingRecords': 'Een moment geduld aub - bezig met laden...',
                    'oPaginate': {
                        'sFirst': 'Eerste',
                        'sLast': 'Laatste',
                        'sNext': 'Volgende',
                        'sPrevious': 'Vorige'
                    },
                    'oAria': {
                        'sSortAscending':  ': activeer om kolom oplopend te sorteren',
                        'sSortDescending': ': activeer om kolom aflopend te sorteren'
                    }
                },
            });
        });
    </script>

@endsection
