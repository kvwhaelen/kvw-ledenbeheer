@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">
        <h2>Kleuter aanpassen</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (!empty($success))
            <div class="alert alert-success">
                <ul>
                    {{ $success }}
                </ul>
            </div>
        @endif

        <div class="card mt-3">
            <div class="card-body">
                <form action="{{ route('preschooler.update', ['preschooler' => $preschooler->id]) }}" method="POST">

                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group field field-text">
                                <label class="control-label" for="firstname">
                                    Voornaam
                                </label>
                                <input name="firstname" id="firstname" class="form-control" type="text" value="{{ $preschooler->member->firstname }}">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group field field-text">
                                <label class="control-label" for="lastname_prefix">
                                    Tussenv.
                                </label>
                                <input name="lastname_prefix" id="lastname_prefix" class="form-control" type="text" value="{{ $preschooler->member->lastname_prefix }}">
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group field field-text">
                                <label class="control-label" for="lastname">
                                    Achternaam
                                </label>
                                <input name="lastname" id="lastname" class="form-control" type="text" value="{{ $preschooler->member->lastname }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-date">
                                <label class="control-label" for="birthday">
                                    Geboortedatum
                                </label>
                                <input name="birthday" id="birthday" class="form-control" type="date" value="{{ $preschooler->member->birthday }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-select">
                                <label class="control-label" for="gender">
                                    Geslacht
                                </label>
                                <select class="form-control" name="gender" id="gender">
                                    <option @if($preschooler->member->gender == 'M') selected @endif value="M">Man</option>
                                    <option @if($preschooler->member->gender == 'V') selected @endif value="V">Vrouw</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group field field-text">
                                <label class="control-label" for="street">
                                    Straat
                                </label>
                                <input name="street" id="street" class="form-control" type="text" value="{{ $preschooler->member->street }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group field field-text">
                                <label class="control-label" for="house_number">
                                    Huisnummer
                                </label>
                                <input name="house_number" id="house_number" class="form-control" type="text" value="{{ $preschooler->member->house_number }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="zipcode">
                                    Postcode
                                </label>
                                <input name="zipcode" id="zipcode" class="form-control" type="text" maxlength="6" value="{{ $preschooler->member->zipcode }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="city">
                                    Plaatsnaam
                                </label>
                                <input name="city" id="city" class="form-control" type="text" value="{{ $preschooler->member->city }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="phone_one">
                                    Telefoonnummer
                                </label>
                                <input name="phone_one" id="phone_one" class="form-control" type="text" value="{{ $preschooler->member->phone_one }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="email">
                                    E-mailadres
                                </label>
                                <input name="email" id="email" class="form-control" type="text" value="{{ $preschooler->member->email }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="gp">
                                    Huisarts
                                </label>
                                <input name="gp" id="gp" class="form-control" type="text" value="{{ $preschooler->member->gp }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="gp_city">
                                    Plaats huisarts
                                </label>
                                <input name="gp_city" id="gp_city" class="form-control" type="text" value="{{ $preschooler->member->gp_city }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="school">
                                    School
                                </label>
                                <input name="school" id="school" class="form-control" type="text" value="{{ $preschooler->school }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="school_group">
                                    Schoolgroep
                                </label>
                                <input name="school_group" id="school_group" class="form-control" type="text" value="{{ $preschooler->school_group }}">
                            </div>
                        </div>
                    </div>

                    <label class="control-label">
                        Ik wil in de groep bij <span class="text-muted small" style="font-weight: normal">(Maximaal 2 namen)</span>
                    </label>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <input name="first_friend_preference" id="first_friend_preference" class="form-control" type="text" value="{{ $preschooler->first_friend_preference }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <input name="second_friend_preference" id="second_friend_preference" class="form-control" type="text" value="{{ $preschooler->second_friend_preference }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group field field-textarea ">
                        <label class="control-label" for="comments">
                            Bijzonderheden
                        </label>
                        <textarea name="comments" class="form-control" id="comments" cols="50" rows="3" value="{{ $preschooler->comments }}"></textarea>
                    </div>

                    <div class="form-actions">
                        <input type="submit" name="Submit" class="btn btn-primary" value="Opslaan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
