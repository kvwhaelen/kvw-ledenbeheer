@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">
        <h2>Kind toevoegen</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif

        <div class="card">
            <div class="card-body">
                <form action="{{ route('child.store') }}" method="POST">

                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-5">
                            <div class="">
                                <label class="control-label" for="firstname">
                                    Voornaam
                                </label>
                                <input name="firstname" id="firstname" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group field field-text">
                                <label class="control-label" for="lastname_prefix">
                                    Tussenv.
                                </label>
                                <input name="lastname_prefix" id="lastname_prefix" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group field field-text">
                                <label class="control-label" for="lastname">
                                    Achternaam
                                </label>
                                <input name="lastname" id="lastname" class="form-control" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-date">
                                <label class="control-label" for="birthday">
                                    Geboortedatum
                                </label>
                                <input name="birthday" id="birthday" class="form-control" type="date">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-select">
                                <label class="control-label" for="gender">
                                    Geslacht
                                </label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="M">Man</option>
                                    <option value="V">Vrouw</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group field field-text">
                                <label class="control-label" for="street">
                                    Straat
                                </label>
                                <input name="street" id="street" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group field field-text">
                                <label class="control-label" for="house_number">
                                    Huisnummer
                                </label>
                                <input name="house_number" id="house_number" class="form-control" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="zipcode">
                                    Postcode
                                </label>
                                <input name="zipcode" id="zipcode" class="form-control" type="text" maxlength="6">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="city">
                                    Plaatsnaam
                                </label>
                                <input name="city" id="city" class="form-control" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="phone_one">
                                    Telefoonnummer
                                </label>
                                <input name="phone_one" id="phone_one" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="email">
                                    E-mailadres
                                </label>
                                <input name="email" id="email" class="form-control" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="gp">
                                    Huisarts
                                </label>
                                <input name="gp" id="gp" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group field field-text">
                                <label class="control-label" for="gp_city">
                                    Plaats huisarts
                                </label>
                                <input name="gp_city" id="gp_city" class="form-control" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group field field-text">
                                <label class="control-label" for="school">
                                    School
                                </label>
                                <input name="school" id="school" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group field field-select">
                                <label class="control-label" for="school_group">
                                    Schoolgroep
                                </label>
                                <select class="form-control" name="school_group" id="school_group">
                                    <option value="3">Groep 3</option>
                                    <option value="4">Groep 4</option>
                                    <option value="5">Groep 5</option>
                                    <option value="6">Groep 6</option>
                                    <option value="7">Groep 7</option>
                                    <option value="8">Groep 8</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group field field-text">
                                <label class="control-label" for="kvw_group">
                                    KVW Groep
                                </label>
                                <input name="kvw_group" id="kvw_group" class="form-control" type="number">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <label class="control-label">
                                Ik wil in de groep bij <span class="text-muted small" style="font-weight: normal">(Maximaal 2 namen)</span>
                            </label>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group field field-text">
                                        <input name="first_friend_preference" id="first_friend_preference" class="form-control" type="text">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group field field-text">
                                        <input name="second_friend_preference" id="second_friend_preference" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group field field-text">
                                <label class="control-label" for="shirtSize">
                                    T-shirt maat
                                </label>

                                <select class="form-control" name="shirt_size" id="shirtSize">
                                    <option value="-">-</option>
                                    <option value="116">116</option>
                                    <option value="128">128</option>
                                    <option value="140">140</option>
                                    <option value="152">152</option>
                                    <option value="164">164</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group field field-textarea ">
                        <label class="control-label" for="comments">
                            Bijzonderheden
                        </label>
                        <textarea name="comments" class="form-control" id="comments" cols="50" rows="3"></textarea>
                    </div>

                    <div class="form-actions">
                        <input type="submit" name="Submit" class="btn btn-primary" value="Opgeven!">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection