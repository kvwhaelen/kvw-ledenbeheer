@extends('layouts.app')

@section('content')

    @include('layouts.navigation')

    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <h2>Kinder overzicht</h2>
            </div>

            <div class="col-md-4 text-right">
                <a href="{{ route('child.create') }}" class="btn btn-outline-primary">
                    <i class="fas fa-plus"></i> Toevoegen
                </a>
            </div>
        </div>

        <div class="table-responsive mt-4">
            <table id="children" class="table table-striped table-bordered nowrap" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Geslacht</th>
                        <th>Voornaam</th>
                        <th>Tussenvoegsel</th>
                        <th>Achternaam</th>
                        <th>Schoolgroep</th>
                        <th>V1</th>
                        <th>V2</th>
                        <th>Straat</th>
                        <th>Huisnummer</th>
                        <th>Postcode</th>
                        <th>Plaatsnaam</th>
                        <th>E-mailadres</th>
                        <th>Telefoon</th>
                        <th>KVW Groep</th>
                        <th>Geboortedatum</th>
                        <th>Huisarts</th>
                        <th>Plaats huisarts</th>
                        <th>T-shirt maat</th>
                        <th>Opmerkingen</th>
                        <th class="dontprint"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($children as $child)
                        <tr>
                            <td>{{ $child->id }}</td>
                            <td>{{ $child->member->gender }}</td>
                            <td>{{ $child->member->firstname }}</td>
                            <td>{{ $child->member->lastname_prefix }}</td>
                            <td>{{ $child->member->lastname }}</td>
                            <td>{{ $child->school_group }}</td>
                            <td>{{ $child->first_friend_preference }}</td>
                            <td>{{ $child->second_friend_preference }}</td>
                            <td>{{ $child->member->street }}</td>
                            <td>{{ $child->member->house_number }}</td>
                            <td>{{ $child->member->zipcode }}</td>
                            <td>{{ $child->member->city }}</td>
                            <td>{{ $child->member->email }}</td>
                            <td>{{ $child->member->phone_one }}</td>
                            <td>{{ $child->kvw_group }}</td>
                            <td>{{ $child->member->birthday }}</td>
                            <td>{{ $child->member->gp }}</td>
                            <td>{{ $child->member->gp_city }}</td>
                            <td>{{ $child->member->shirt_size }}</td>
                            <td>{{ $child->comments }}</td>
                            <td class="dontprint"><a href="{{ route('child.edit', ['child' => $child->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('table#children').DataTable({
                stateSave: true,
                dom: 'Bfirtlp',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible',
                        },
                    },
                    {
                        extend: 'colvis',
                        text: 'Filter',
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ]
                    }
                ],
                'columnDefs': [
                    {
                        'orderable': false,
                        'targets': 16,
                    }
                ],
                'fixedColumns':   {
                    'rightColumns': 1,
                },
                'scrollX': true,
                'language': {
                    'sProcessing': 'Bezig...',
                    'sLengthMenu': '_MENU_ resultaten weergeven',
                    'sZeroRecords': 'Geen resultaten gevonden',
                    'sInfo': '_START_ tot _END_ van _TOTAL_ resultaten',
                    'sInfoEmpty': 'Geen resultaten om weer te geven',
                    'sInfoFiltered': ' (gefilterd uit _MAX_ resultaten)',
                    'sInfoPostFix': '',
                    'sSearch': 'Zoeken:',
                    'sEmptyTable': 'Geen resultaten aanwezig in de tabel',
                    'sInfoThousands': '.',
                    'sLoadingRecords': 'Een moment geduld aub - bezig met laden...',
                    'oPaginate': {
                        'sFirst': 'Eerste',
                        'sLast': 'Laatste',
                        'sNext': 'Volgende',
                        'sPrevious': 'Vorige'
                    },
                    'oAria': {
                        'sSortAscending':  ': activeer om kolom oplopend te sorteren',
                        'sSortDescending': ': activeer om kolom aflopend te sorteren'
                    }
                },
            });
        });
    </script>

@endsection
